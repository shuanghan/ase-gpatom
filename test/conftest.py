import numpy as np
import pytest
from ase.utils import workdir


@pytest.fixture(autouse=True)
def in_tempdir(tmp_path):
    with workdir(tmp_path):
        yield


def raise_rng_error(*args, **kwargs):
    raise RuntimeError('One of the tests is using unseeded random numbers, '
                       'please fixme')


@pytest.fixture(autouse=True, scope='session')
def hack_rng():
    # Replace the default RNG with something that raises an error, should
    # tests accidentally use non-seeded random numbers.

    dct = vars(np.random)
    keys = list(dct)
    for key in keys:
        if key == 'RandomState':
            continue

        value = dct[key]
        # We monkeypatch each callable in order to raise
        # error if someone calls it.
        if callable(value):
            dct[key] = raise_rng_error
